<!DOCTYPE html>
<html>
<head>
<title>Horoscope</title>
<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body class="bg-primary">
	<h1 class="text-center text-white my-5">Horoscope</h1>
		<div class="col-lg-4 offset-lg-4">
			<div class="card text-white bg-dark mb-3">
				<h1 class="text-center text-white">Welcome!</h1>
				
				<form action="controllers/horoscopeprocess.php" method="POST">
					
					<div class="form-group text-center">
						<label for="name">Name:</label>
						<input type="text" name="name">
					</div>

					<div class="form-group text-center">
						<label for="birthDate">Day of Birth:(dd)</label>
						<input type="number" max="31" min="1" name="birthDate">
					</div>
					
					<div class="form-group text-center">
						<label for="birthMonth">Month of Birth:(mm)</label>
						<input type="number" max="12" min="1" name="birthMonth">
					</div>

					<div class="text-center">
						<button class="btn btn-success">Find Zodiac Sign</button>
					</div>
				</form>
			</div>
		</div>
</body>
</html>
